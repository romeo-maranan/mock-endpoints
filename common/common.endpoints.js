const express = require('express');
const router = express.Router();

router.get('/no-content', function (req, res) {
    res.status(204).end();
});

module.exports = router;