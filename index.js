const express = require('express');

const common = require('./common/common.endpoints');

const app = express();
app.use('/common', common);

app.listen(3210, function() {
    console.log('Mock Enpoints running at port 3210...!');
});
